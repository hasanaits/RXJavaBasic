package com.hasan.rxjavabasic.ui.splash;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.hasan.rxjavabasic.R;


import org.reactivestreams.Subscriber;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class SplashActivity extends AppCompatActivity {

    @BindView(R.id.tvText)
    TextView tvText;
    @BindView(R.id.btnNext)
    Button btnNext;

    Observable<String> mObservable;
    Observer<String> mObserver;

    private static final String TAG = "SplashActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        mObservable = Observable.just("Splash Page","Splash Page2");

        mObserver = new Observer<String>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(String s) {
                Log.e(TAG, "onNext: "+s );
                tvText.setText(s);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {
                Log.e(TAG, "onComplete: ");
            }
        };
    }


    @OnClick(R.id.btnNext)
    void onClick(){
        mObservable.subscribe(mObserver);
    }
}
